/* globals describe, before, after, it */

'use strict';

describe('#auth-http-spec', function() {
  var chai = require('chai');
  var expect = chai.expect;

  var merge = require('lodash.merge');

  var validator = new (require('jsonschema').Validator)();
  require('jsonschema-extra')(validator);

  var auth = require('@knoxxnxt/auth')();
  var spec = require('./../spec')(auth);
  var specSchema = require('./schema')(auth);

  var state = { fns: [], paths: {} };

  function createTests(id, spec) {
    describe('#' + id, function() {
      before(function() {
        state.fns.push(spec.fn);
      });

      after(function() {
        state.paths[spec.method + ' ' + spec.path] = true;
      });

      if ('logout' !== id && 'resume' !== id) {
        it('should pass schema', function() {
          var res = validator.validate(spec, specSchema);
          expect(res.valid).to.equal(true, res.errors);
        });
      }

      it('should be a unique path', function() {
        expect(state.paths).to.not.have.property(spec.method + ' ' + spec.path);
      });

      it('should flag all the properties in the method schema not available on the route schema', function() {
        var s = spec.schema, p = 'properties';

        if (s) {
          var props = merge({}, (s.body && s.body[p]) || {}, (s.params && s.params[p]) || {}, (s.query && s.query[p]) || {});

          for (var prop in spec.fn.schema.properties) {
            if (!(prop in props)) {
              console.warn('Property `%s` not found in route `%s`', prop, id);
            }
          }
        }
      });
    });
  }

  for (let i in spec) {
    createTests(i, spec[i]);
  }

  describe('#all', function() {
    for (let i in auth.methods) {
      it(`should have a route for ${i}`, function() {
        let method = auth.methods[i];
        expect(state.fns).to.contain(method);
      });
    }
  });
});
