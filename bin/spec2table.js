#!/usr/bin/env node

'use strict';

var fs = require('fs');

var auth = require('@knoxxnxt/auth')();
var spec = require('../spec')(auth);

var markdown = '';

for (let id in spec) {
  let row = spec[id];

  markdown += '\n### ' + id + '\n';
  // description
  markdown += '\n' + row.description + '\n';
  // route
  markdown += '\n#### URL\n';
  markdown += '\n```http\n' + row.method.toUpperCase() + ' ' + row.path + '\n```\n';
  // schema
  markdown += '\n#### Request Parameters\n';
  markdown += '\n```json\n' + JSON.stringify(row.schema, null, 2) + '\n```\n';
  // state
  markdown += '\n#### Access control\n';
  markdown += '\nWho can access this method?\n';
  markdown += '\n```json\n' + JSON.stringify(row.state, null, 2) + '\n```\n';
  // line break
  markdown += '\n\n';
}

fs.writeFileSync(__dirname + '/out/api.md', markdown, 'utf8');

console.info('\n\nDone. Output in ./out/api.md\n');