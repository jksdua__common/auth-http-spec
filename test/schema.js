'use strict';

var argsSchema = {
  type: 'object',
  properties: {
    properties: {
      type: 'object',
      required: true,
      validate: function(ins) {
        for (let i in ins) {
          if ('object' !== typeof ins[i]) {
            return 'contains an invalid property: ' + i;
          }
        }
      }
    }
  }
};

module.exports = function(auth) {
  return {
    type: 'object',
    properties: {
      description: { type: 'string', required: true },
      path: { type: 'string', required: true },
      method: { type: 'string', required: true, enum: ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'] },
      schema: {
        type: 'object',
        required: true,
        properties: {
          params: argsSchema,
          query: argsSchema,
          body: argsSchema
        },
        additionalProperties: false
      },
      state: {
        type: 'object',
        required: true,
        properties: {
          authenticated: { type: 'boolean' },
          role: { type: 'string', enum: auth.constants.ROLES },
          status: { type: 'string', enum: auth.constants.STATUSES }
        }
      },
      fn: {
        type: 'generatorFunction',
        required: true
      },
      session: {
        type: 'object',
        required: false,
        oneOf: [
          {
            properties: {
              set: { type: 'boolean', required: true, enum: [true] }
            },
            additionalProperties: false
          },
          {
            properties: {
              unset: { type: 'boolean', required: true, enum: [true] }
            },
            additionalProperties: false
          },
          {
            properties: {
              resume: { type: 'boolean', required: true, enum: [true] }
            },
            additionalProperties: false
          },
          {
            properties: {
              args: {
                type: 'array',
                required: true,
                minItems: 1,
                items: { type: 'string', required: true }
              }
            },
            additionalProperties: false
          }
        ]
      }
    }
  };
};