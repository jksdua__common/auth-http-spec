'use strict';

var SUPPORTED_VERSIONS = '~7.0.x';

var merge = require('lodash.merge');
var assert = require('assert');

function getSchema(schema, props) {
  var a = {
    properties: {}
  };

  Object.keys(schema).forEach(function(prop) {
    if ('properties' !== prop) {
      a[prop] = schema[prop];
    }
  });

  props.forEach(function(prop) {
    if (Array.isArray(prop) && 2 === prop.length) {
      // dont overwrite auth method schema
      a.properties[prop[0]] = merge({}, schema.properties[prop[0]], prop[1]);
    } else if ('string' === typeof prop) {
      a.properties[prop] = schema.properties[prop];
    } else {
      throw new Error('Invalid property configuration: ' + prop);
    }
  });

  return a;
}

module.exports = function(auth) {
  assert(auth.isSupported(SUPPORTED_VERSIONS));

  var ROLE = auth.constants.ROLE;
  var STATUS = auth.constants.STATUS;

  var httpSpec = {};

  httpSpec.get = {
    description: 'Fetch a particular user by email',
    path: '/user/:email',
    method: 'GET',
    schema: { params: getSchema(auth.methods.get.schema, ['email']) },
    state: { role: ROLE.USER_ADMIN, authenticated: true, status: STATUS.ENABLED },
    fn: auth.methods.get
  };

  httpSpec.all = {
    description: 'Fetch all the users based on arguments passed',
    path: '/user',
    method: 'GET',
    schema: { query: auth.methods.all.schema },
    state: { role: ROLE.USER_ADMIN, authenticated: true, status: STATUS.ENABLED },
    fn: auth.methods.all
  };

  httpSpec.login = {
    description: 'Login a user',
    path: '/login',
    method: 'POST',
    schema: { body: getSchema(auth.methods.login.schema, ['email', 'pass']) },
    state: { authenticated: false },
    session: { set: true },
    fn: auth.methods.login
  };

  httpSpec.logout = {
    description: 'Logout a user',
    path: '/logout',
    method: 'POST',
    state: { authenticated: true },
    session: { unset: true }
  };

  httpSpec.resume = {
    description: 'Resume an existing user session',
    path: '/resume',
    method: 'GET',
    // user could be in any state
    state: { skip: true },
    session: { resume: true }
  };

  httpSpec.register = {
    description: 'Register a user',
    path: '/register',
    method: 'POST',
    schema: {
      body: getSchema(auth.methods.register.schema, ['email', 'pass'])
    },
    state: { authenticated: false },
    session: { set: true },
    fn: auth.methods.register
  };

  httpSpec.activate = {
    description: 'Activate a registered user',
    path: '/activate',
    method: 'POST',
    schema: {
      body: getSchema(
        auth.methods.activate.schema,
        ['email', ['token', { required: true }]]
      )
    },
    state: { authenticated: false },
    session: { set: true },
    fn: auth.methods.activate
  };

  httpSpec.resendActicationEmail = {
    description: 'Resend activation email to registered user',
    path: '/resend-activation',
    method: 'POST',
    schema: {
      body: getSchema(auth.methods.resendActivationEmail.schema, ['email'])
    },
    state: { authenticated: false },
    fn: auth.methods.resendActivationEmail
  };

  httpSpec.resendActivationEmail2 = {
    description: 'Resend activation email to registered user',
    path: '/resend-activation-2',
    method: 'POST',
    schema: {},
    state: { authenticated: true, status: STATUS.REGISTERED },
    // add email to the arguments object when sending the request
    session: { args: ['email'] },
    fn: auth.methods.resendActivationEmail
  };

  httpSpec.resendInvitationEmail = {
    description: 'Resend activation email to registered user',
    path: '/resend-invitation',
    method: 'POST',
    schema: {
      body: getSchema(auth.methods.resendInvitationEmail.schema, ['email'])
    },
    state: { authenticated: false },
    fn: auth.methods.resendInvitationEmail
  };

  httpSpec.adminActivate = {
    description: '(Admin) Activate a registered user',
    path: '/user/:email/activate',
    method: 'POST',
    schema: {
      body: getSchema(auth.methods.activate.schema, ['token', 'skipTokenVerification']),
      params: getSchema(auth.methods.activate.schema, ['email'])
    },
    state: { role: ROLE.USER_ADMIN, authenticated: true, status: STATUS.ENABLED },
    session: { set: true },
    fn: auth.methods.activate
  };

  httpSpec.invite = {
    description: 'Invite a user. Sends an activation email to the user for activating their account.',
    path: '/invite',
    method: 'POST',
    schema: {
      body: getSchema(auth.methods.invite.schema, ['email', 'roles', 'properties'])
    },
    state: { authenticated: true, role: ROLE.USER_ADMIN, status: STATUS.ENABLED },
    fn: auth.methods.invite
  };

  httpSpec.acceptInvite = {
    description: 'Accept an invitation',
    path: '/invite/accept',
    method: 'POST',
    schema: {
      body: getSchema(
        auth.methods.acceptInvite.schema,
        ['email', 'pass', ['token', { required: true }]]
      )
    },
    state: { authenticated: false },
    session: { set: true },
    fn: auth.methods.acceptInvite
  };

  httpSpec.adminAcceptInvite = {
    description: 'Accept an invitation',
    path: '/user/:email/invite/accept',
    method: 'POST',
    schema: {
      params: getSchema(auth.methods.acceptInvite.schema, ['email']),
      body: getSchema(auth.methods.acceptInvite.schema, ['pass', 'token', 'skipTokenVerification'])
    },
    state: { authenticated: true, role: ROLE.USER_ADMIN, status: STATUS.ENABLED },
    session: { set: true },
    fn: auth.methods.acceptInvite
  };

  httpSpec.rejectInvite = {
    description: 'Reject an invitation',
    path: '/invite/reject',
    method: 'POST',
    schema: {
      body: getSchema(
        auth.methods.rejectInvite.schema,
        ['email', ['token', { required: true }]]
      )
    },
    state: { authenticated: false },
    fn: auth.methods.rejectInvite
  };

  httpSpec.adminRejectInvite = {
    description: 'Reject an invitation',
    path: '/user/:email/invite/reject',
    method: 'POST',
    schema: {
      params: getSchema(auth.methods.rejectInvite.schema, ['email']),
      body: getSchema(auth.methods.rejectInvite.schema, ['token', 'skipTokenVerification'])
    },
    state: { authenticated: true, role: ROLE.USER_ADMIN, status: STATUS.ENABLED },
    fn: auth.methods.rejectInvite
  };

  httpSpec.getProperties = {
    description: 'Get the properties of a user',
    path: '/user/:email/properties',
    method: 'GET',
    schema: {
      params: getSchema(auth.methods.getProperties.schema, ['email'])
    },
    state: { role: ROLE.USER_ADMIN, authenticated: true, status: STATUS.ENABLED },
    fn: auth.methods.getProperties
  };

  httpSpec.setProperties = {
    description: 'Set the properties of a user',
    path: '/user/:email/properties',
    method: 'PUT',
    schema: {
      params: getSchema(auth.methods.setProperties.schema, ['email']),
      body: getSchema(auth.methods.setProperties.schema, ['properties'])
    },
    state: { role: ROLE.USER_ADMIN, authenticated: true, status: STATUS.ENABLED },
    fn: auth.methods.setProperties
  };

  httpSpec.changePassword = {
    description: 'Change currently logged in user\'s password',
    path: '/password/change',
    method: 'POST',
    schema: {
      body: getSchema(auth.methods.changePassword.schema, ['oldPass', 'newPass'])
    },
    state: { role: ROLE.USER, authenticated: true, status: STATUS.ENABLED },
    // add email to the arguments object when sending the request
    session: { args: ['email'] },
    fn: auth.methods.changePassword
  };

  httpSpec.adminChangePassword = {
    description: '(Admin) Change user\'s password',
    path: '/user/:email/password/change',
    method: 'POST',
    schema: {
      params: getSchema(auth.methods.changePassword.schema, ['email']),
      body: getSchema(auth.methods.changePassword.schema, ['oldPass', 'newPass'])
    },
    state: { role: ROLE.USER_ADMIN, authenticated: true, status: STATUS.ENABLED },
    fn: auth.methods.changePassword
  };

  httpSpec.resetRequest = {
    description: 'Request a password reset',
    path: '/password/reset-request',
    method: 'POST',
    schema: {
      body: getSchema(auth.methods.resetRequest.schema, ['email'])
    },
    state: { authenticated: false },
    fn: auth.methods.resetRequest
  };

  httpSpec.adminResetRequest = {
    description: '(Admin) Request a password reset',
    path: '/user/:email/password/reset-request',
    method: 'POST',
    schema: {
      params: getSchema(auth.methods.resetRequest.schema, ['email'])
    },
    state: { role: ROLE.USER_ADMIN, authenticated: true, status: STATUS.ENABLED },
    fn: auth.methods.resetRequest
  };

  httpSpec.resetResponse = {
    description: 'Process a password reset',
    path: '/password/reset-response',
    method: 'POST',
    schema: {
      body: getSchema(
        auth.methods.resetResponse.schema,
        ['email', 'pass', ['token', { required: true }]]
      )
    },
    state: { authenticated: false },
    session: { set: true },
    fn: auth.methods.resetResponse
  };

  httpSpec.adminResetResponse = {
    description: '(Admin) Process a password reset',
    path: '/user/:email/password/reset-response',
    method: 'POST',
    schema: {
      params: getSchema(auth.methods.resetResponse.schema, ['email']),
      body: getSchema(auth.methods.resetResponse.schema, ['pass', 'token', 'skipTokenVerification'])
    },
    state: { role: ROLE.USER_ADMIN, authenticated: true, status: STATUS.ENABLED },
    fn: auth.methods.resetResponse
  };

  httpSpec.disable = {
    description: 'Disable a user',
    path: '/user/:email/disable',
    method: 'POST',
    schema: {
      params: getSchema(auth.methods.disable.schema, ['email'])
    },
    state: { role: ROLE.USER_ADMIN, authenticated: true, status: STATUS.ENABLED },
    fn: auth.methods.disable
  };

  httpSpec.enable = {
    description: 'Enable a user',
    path: '/user/:email/enable',
    method: 'POST',
    schema: {
      params: getSchema(auth.methods.enable.schema, ['email'])
    },
    state: { role: ROLE.USER_ADMIN, authenticated: true, status: STATUS.ENABLED },
    fn: auth.methods.enable
  };

  httpSpec.insert = {
    description: 'Low-level method for inserting a user. **Not recommended** - use `invite` or `register` instead',
    path: '/raw/insert',
    method: 'POST',
    schema: {
      body: auth.methods.insert.schema
    },
    state: { authenticated: true, role: ROLE.USER_ADMIN, status: STATUS.ENABLED },
    fn: auth.methods.insert
  };

  httpSpec.update = {
    description: 'Low-level method for updating a user. **Not recommended** - use `setProperties` instead',
    path: '/raw/update',
    method: 'POST',
    schema: {
      body: auth.methods.update.schema
    },
    state: { authenticated: true, role: ROLE.USER_ADMIN, status: STATUS.ENABLED },
    fn: auth.methods.update
  };

  httpSpec.remove = {
    description: 'Low-level method for permanently removing a user. **Not recommended** - use `disable` instead',
    path: '/raw/remove',
    method: 'POST',
    schema: {
      body: auth.methods.remove.schema
    },
    state: { authenticated: true, role: ROLE.USER_ADMIN, status: STATUS.ENABLED },
    fn: auth.methods.remove
  };

  return httpSpec;
};
