# auth-http-spec

HTTP spec for @knoxxnxt/auth


Versions are kept consistent with @knoxxnxt/auth to make it easier to track compatibility.


## Installation

```bash
$ npm i @knoxxnxt/auth-http-spec
```


## API



### get

Fetch a particular user by email

#### URL

```http
GET /user/:email
```

#### Request Parameters

```json
{
  "params": {
    "properties": {
      "email": {
        "type": "string",
        "format": "email",
        "required": true
      }
    },
    "additionalProperties": false
  }
}
```

#### Access control

Who can access this method?

```json
{
  "role": "user admin",
  "authenticated": true,
  "status": "enabled"
}
```



### all

Fetch all the users based on arguments passed

#### URL

```http
GET /user
```

#### Request Parameters

```json
{
  "query": {
    "required": false,
    "properties": {},
    "additionalProperties": false
  }
}
```

#### Access control

Who can access this method?

```json
{
  "role": "user admin",
  "authenticated": true,
  "status": "enabled"
}
```



### login

Login a user

#### URL

```http
POST /login
```

#### Request Parameters

```json
{
  "body": {
    "properties": {
      "email": {
        "type": "string",
        "format": "email",
        "required": true
      },
      "pass": {
        "type": "string",
        "minLength": 8,
        "maxLength": 100,
        "required": true
      }
    },
    "additionalProperties": false
  }
}
```

#### Access control

Who can access this method?

```json
{
  "authenticated": false
}
```



### logout

Logout a user

#### URL

```http
POST /logout
```

#### Request Parameters

```json
undefined
```

#### Access control

Who can access this method?

```json
{
  "authenticated": true
}
```



### resume

Resume an existing user session

#### URL

```http
GET /resume
```

#### Request Parameters

```json
undefined
```

#### Access control

Who can access this method?

```json
{
  "skip": true
}
```



### register

Register a user

#### URL

```http
POST /register
```

#### Request Parameters

```json
{
  "body": {
    "properties": {
      "email": {
        "type": "string",
        "format": "email",
        "required": true
      },
      "pass": {
        "type": "string",
        "minLength": 8,
        "maxLength": 100,
        "required": true
      }
    },
    "additionalProperties": false
  }
}
```

#### Access control

Who can access this method?

```json
{
  "authenticated": false
}
```



### activate

Activate a registered user

#### URL

```http
POST /activate
```

#### Request Parameters

```json
{
  "body": {
    "properties": {
      "email": {
        "type": "string",
        "format": "email",
        "required": true
      },
      "token": {
        "type": "string",
        "minLength": 64,
        "maxLength": 64,
        "required": true
      }
    },
    "additionalProperties": false
  }
}
```

#### Access control

Who can access this method?

```json
{
  "authenticated": false
}
```



### resendActicationEmail

Resend activation email to registered user

#### URL

```http
POST /resend-activation
```

#### Request Parameters

```json
{
  "body": {
    "properties": {
      "email": {
        "type": "string",
        "format": "email",
        "required": true
      }
    },
    "additionalProperties": false
  }
}
```

#### Access control

Who can access this method?

```json
{
  "authenticated": false
}
```



### resendActivationEmail2

Resend activation email to registered user

#### URL

```http
POST /resend-activation-2
```

#### Request Parameters

```json
{}
```

#### Access control

Who can access this method?

```json
{
  "authenticated": true,
  "status": "registered"
}
```



### resendInvitationEmail

Resend activation email to registered user

#### URL

```http
POST /resend-invitation
```

#### Request Parameters

```json
{
  "body": {
    "properties": {
      "email": {
        "type": "string",
        "format": "email",
        "required": true
      }
    },
    "additionalProperties": false
  }
}
```

#### Access control

Who can access this method?

```json
{
  "authenticated": false
}
```



### adminActivate

(Admin) Activate a registered user

#### URL

```http
POST /user/:email/activate
```

#### Request Parameters

```json
{
  "body": {
    "properties": {
      "token": {
        "type": "string",
        "minLength": 64,
        "maxLength": 64,
        "required": false
      },
      "skipTokenVerification": {
        "type": "boolean",
        "required": false,
        "default": false
      }
    },
    "additionalProperties": false
  },
  "params": {
    "properties": {
      "email": {
        "type": "string",
        "format": "email",
        "required": true
      }
    },
    "additionalProperties": false
  }
}
```

#### Access control

Who can access this method?

```json
{
  "role": "user admin",
  "authenticated": true,
  "status": "enabled"
}
```



### invite

Invite a user. Sends an activation email to the user for activating their account.

#### URL

```http
POST /invite
```

#### Request Parameters

```json
{
  "body": {
    "properties": {
      "email": {
        "type": "string",
        "format": "email",
        "required": true
      },
      "roles": {
        "type": "array",
        "required": false,
        "minItems": 1,
        "items": {
          "type": "string",
          "required": true
        }
      },
      "properties": {
        "type": "object",
        "required": false
      }
    },
    "additionalProperties": false
  }
}
```

#### Access control

Who can access this method?

```json
{
  "authenticated": true,
  "role": "user admin",
  "status": "enabled"
}
```



### acceptInvite

Accept an invitation

#### URL

```http
POST /invite/accept
```

#### Request Parameters

```json
{
  "body": {
    "properties": {
      "email": {
        "type": "string",
        "format": "email",
        "required": true
      },
      "pass": {
        "type": "string",
        "minLength": 8,
        "maxLength": 100,
        "required": true
      },
      "token": {
        "type": "string",
        "minLength": 64,
        "maxLength": 64,
        "required": true
      }
    },
    "additionalProperties": false
  }
}
```

#### Access control

Who can access this method?

```json
{
  "authenticated": false
}
```



### adminAcceptInvite

Accept an invitation

#### URL

```http
POST /user/:email/invite/accept
```

#### Request Parameters

```json
{
  "params": {
    "properties": {
      "email": {
        "type": "string",
        "format": "email",
        "required": true
      }
    },
    "additionalProperties": false
  },
  "body": {
    "properties": {
      "pass": {
        "type": "string",
        "minLength": 8,
        "maxLength": 100,
        "required": true
      },
      "token": {
        "type": "string",
        "minLength": 64,
        "maxLength": 64,
        "required": false
      },
      "skipTokenVerification": {
        "type": "boolean",
        "required": false,
        "default": false
      }
    },
    "additionalProperties": false
  }
}
```

#### Access control

Who can access this method?

```json
{
  "authenticated": true,
  "role": "user admin",
  "status": "enabled"
}
```



### rejectInvite

Reject an invitation

#### URL

```http
POST /invite/reject
```

#### Request Parameters

```json
{
  "body": {
    "properties": {
      "email": {
        "type": "string",
        "format": "email",
        "required": true
      },
      "token": {
        "type": "string",
        "minLength": 64,
        "maxLength": 64,
        "required": true
      }
    },
    "additionalProperties": false
  }
}
```

#### Access control

Who can access this method?

```json
{
  "authenticated": false
}
```



### adminRejectInvite

Reject an invitation

#### URL

```http
POST /user/:email/invite/reject
```

#### Request Parameters

```json
{
  "params": {
    "properties": {
      "email": {
        "type": "string",
        "format": "email",
        "required": true
      }
    },
    "additionalProperties": false
  },
  "body": {
    "properties": {
      "token": {
        "type": "string",
        "minLength": 64,
        "maxLength": 64,
        "required": false
      },
      "skipTokenVerification": {
        "type": "boolean",
        "required": false,
        "default": false
      }
    },
    "additionalProperties": false
  }
}
```

#### Access control

Who can access this method?

```json
{
  "authenticated": true,
  "role": "user admin",
  "status": "enabled"
}
```



### getProperties

Get the properties of a user

#### URL

```http
GET /user/:email/properties
```

#### Request Parameters

```json
{
  "params": {
    "properties": {
      "email": {
        "type": "string",
        "format": "email",
        "required": true
      }
    },
    "additionalProperties": false
  }
}
```

#### Access control

Who can access this method?

```json
{
  "role": "user admin",
  "authenticated": true,
  "status": "enabled"
}
```



### setProperties

Set the properties of a user

#### URL

```http
PUT /user/:email/properties
```

#### Request Parameters

```json
{
  "params": {
    "properties": {
      "email": {
        "type": "string",
        "format": "email",
        "required": true
      }
    },
    "additionalProperties": false
  },
  "body": {
    "properties": {
      "properties": {
        "type": "object",
        "required": false
      }
    },
    "additionalProperties": false
  }
}
```

#### Access control

Who can access this method?

```json
{
  "role": "user admin",
  "authenticated": true,
  "status": "enabled"
}
```



### changePassword

Change currently logged in user's password

#### URL

```http
POST /password/change
```

#### Request Parameters

```json
{
  "body": {
    "properties": {
      "oldPass": {
        "type": "string",
        "minLength": 8,
        "maxLength": 100,
        "required": true
      },
      "newPass": {
        "type": "string",
        "minLength": 8,
        "maxLength": 100,
        "required": true
      }
    },
    "additionalProperties": false
  }
}
```

#### Access control

Who can access this method?

```json
{
  "role": "user",
  "authenticated": true,
  "status": "enabled"
}
```



### adminChangePassword

(Admin) Change user's password

#### URL

```http
POST /user/:email/password/change
```

#### Request Parameters

```json
{
  "params": {
    "properties": {
      "email": {
        "type": "string",
        "format": "email",
        "required": true
      }
    },
    "additionalProperties": false
  },
  "body": {
    "properties": {
      "oldPass": {
        "type": "string",
        "minLength": 8,
        "maxLength": 100,
        "required": true
      },
      "newPass": {
        "type": "string",
        "minLength": 8,
        "maxLength": 100,
        "required": true
      }
    },
    "additionalProperties": false
  }
}
```

#### Access control

Who can access this method?

```json
{
  "role": "user admin",
  "authenticated": true,
  "status": "enabled"
}
```



### resetRequest

Request a password reset

#### URL

```http
POST /password/reset-request
```

#### Request Parameters

```json
{
  "body": {
    "properties": {
      "email": {
        "type": "string",
        "format": "email",
        "required": true
      }
    },
    "additionalProperties": false
  }
}
```

#### Access control

Who can access this method?

```json
{
  "authenticated": false
}
```



### adminResetRequest

(Admin) Request a password reset

#### URL

```http
POST /user/:email/password/reset-request
```

#### Request Parameters

```json
{
  "params": {
    "properties": {
      "email": {
        "type": "string",
        "format": "email",
        "required": true
      }
    },
    "additionalProperties": false
  }
}
```

#### Access control

Who can access this method?

```json
{
  "role": "user admin",
  "authenticated": true,
  "status": "enabled"
}
```



### resetResponse

Process a password reset

#### URL

```http
POST /password/reset-response
```

#### Request Parameters

```json
{
  "body": {
    "properties": {
      "email": {
        "type": "string",
        "format": "email",
        "required": true
      },
      "pass": {
        "type": "string",
        "minLength": 8,
        "maxLength": 100,
        "required": true
      },
      "token": {
        "type": "string",
        "minLength": 64,
        "maxLength": 64,
        "required": true
      }
    },
    "additionalProperties": false
  }
}
```

#### Access control

Who can access this method?

```json
{
  "authenticated": false
}
```



### adminResetResponse

(Admin) Process a password reset

#### URL

```http
POST /user/:email/password/reset-response
```

#### Request Parameters

```json
{
  "params": {
    "properties": {
      "email": {
        "type": "string",
        "format": "email",
        "required": true
      }
    },
    "additionalProperties": false
  },
  "body": {
    "properties": {
      "pass": {
        "type": "string",
        "minLength": 8,
        "maxLength": 100,
        "required": true
      },
      "token": {
        "type": "string",
        "minLength": 64,
        "maxLength": 64,
        "required": false
      },
      "skipTokenVerification": {
        "type": "boolean",
        "required": false,
        "default": false
      }
    },
    "additionalProperties": false
  }
}
```

#### Access control

Who can access this method?

```json
{
  "role": "user admin",
  "authenticated": true,
  "status": "enabled"
}
```



### disable

Disable a user

#### URL

```http
POST /user/:email/disable
```

#### Request Parameters

```json
{
  "params": {
    "properties": {
      "email": {
        "type": "string",
        "format": "email",
        "required": true
      }
    },
    "additionalProperties": false
  }
}
```

#### Access control

Who can access this method?

```json
{
  "role": "user admin",
  "authenticated": true,
  "status": "enabled"
}
```



### enable

Enable a user

#### URL

```http
POST /user/:email/enable
```

#### Request Parameters

```json
{
  "params": {
    "properties": {
      "email": {
        "type": "string",
        "format": "email",
        "required": true
      }
    },
    "additionalProperties": false
  }
}
```

#### Access control

Who can access this method?

```json
{
  "role": "user admin",
  "authenticated": true,
  "status": "enabled"
}
```



### insert

Low-level method for inserting a user. **Not recommended** - use `invite` or `register` instead

#### URL

```http
POST /raw/insert
```

#### Request Parameters

```json
{
  "body": {
    "properties": {
      "email": {
        "type": "string",
        "format": "email",
        "required": true
      },
      "pass": {
        "type": "string",
        "minLength": 8,
        "maxLength": 100,
        "required": true
      },
      "roles": {
        "type": "object",
        "required": false
      },
      "properties": {
        "type": "object",
        "required": false
      }
    }
  }
}
```

#### Access control

Who can access this method?

```json
{
  "authenticated": true,
  "role": "user admin",
  "status": "enabled"
}
```



### update

Low-level method for updating a user. **Not recommended** - use `setProperties` instead

#### URL

```http
POST /raw/update
```

#### Request Parameters

```json
{
  "body": {
    "properties": {},
    "additionalProperties": true
  }
}
```

#### Access control

Who can access this method?

```json
{
  "authenticated": true,
  "role": "user admin",
  "status": "enabled"
}
```



### remove

Low-level method for permanently removing a user. **Not recommended** - use `disable` instead

#### URL

```http
POST /raw/remove
```

#### Request Parameters

```json
{
  "body": {
    "properties": {
      "email": {
        "type": "string",
        "format": "email",
        "required": true
      }
    },
    "additionalProperties": false
  }
}
```

#### Access control

Who can access this method?

```json
{
  "authenticated": true,
  "role": "user admin",
  "status": "enabled"
}
```


## Implementations
- [koa-auth](https://gitlab.com/jksdua__common/koa-auth) for [koa-framework](https://www.npmjs.com/package/koa-framework)


## Changelog

### v6.1.0-1.0.2 (10 February 2016)
- Minor updates to some functions

### v6.1.0-1.0.1 (3 February 2016)
- Added more restrictive schema for certain routes

### v6.1.0-1.0.0 (7 December 2015)
- Updated to work with auth 6.1.x
  - Added new routes for resending activation and invitation emails

### v6.0.0-1.0.2 (29 July 2015)
- Fixed issue where certain schema properties were being omitted

### v6.0.0-1.0.1 (29 July 2015)
- Fixed issue where original auth method schema was being overwritten

### v6.0.0-1.0.0 (23 July 2015)
- Updated @knoxxnxt/auth
- Added two new methods - `resendActivationEmail`, `resendActivationEmail2`
- Changed name to @knoxxnxt/auth-http-spec

### v5.0.0-2.0.1 (23 April 2015)
- Fixed state property for `/resume` session

### v5.0.0-2.0.0 (10 April 2015)
- Added resume session spec `/resume`

### v5.0.0-1.0.1 (16 February 2015)
- Schema bug fix

### v5.0.0-1.0.0 (15 February 2015)
- Upgraded `auth` support to version 5.0.x

### v4.0.0 (28 January 2015)
- Upgraded `auth` support to version 4.0.x
- Added additional properties to describe session changes

### v3.0.0 (22 January 2015)
- Initial commit
